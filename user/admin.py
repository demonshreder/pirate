from django.contrib import admin
from user.models import UserProfile,Communities,FriendRequests
# Register your models here.
admin.site.register(UserProfile)
admin.site.register(Communities)
admin.site.register(FriendRequests)