#URL & shortcuts
from django.shortcuts import render, redirect, get_object_or_404, render_to_response
from django.http import HttpResponse
#DB Imports
from django.contrib.auth.models import User
from user.models import UserProfile, Communities, FriendRequests
from files.models import files
#Authentication
from django.contrib.auth import authenticate
from django.contrib import auth
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_POST
from django.contrib.auth.password_validation import validate_password, password_validators_help_text_html
from django.core.exceptions import ValidationError
#Querying
from django.db.models import Q

#Static links
def policy(request):
	return render(request,"user/policy.html")

def about(request):
	return render(request,"user/about.html")

def updates(request):
	return render(request,"user/updates.html")

def test2(request):
	return render(request,"user/about.html")

#Basic User stuff
def register(request):
	if request.method == "POST":
		#Usual test cases
		if User.objects.filter(username=request.POST['user_reg']).exists():
			return render(request, 'user/login.html',{'usr':" <h4 class='text-danger'>Username already exists.</h4>"})
		elif User.objects.filter(email=request.POST['email']).exists():
			return render(request, 'user/login.html',{'usr':" <h4 class='text-danger'>Email already exists.</h4>"})
		elif request.POST['password'] != request.POST['password2']:
			return render(request, 'user/login.html',{'pwd':" <h4 class='text-danger'>Both the passwords don't match.</h4>"})
		else:
			#Validate the password
			try:
				validate_password(request.POST['password'])
			except ValidationError as e:
				return render(request, 'user/login.html',{'pwd':password_validators_help_text_html()})
			#Yay, new user
			user = User.objects.create_user(request.POST['user_reg'],request.POST['email'],request.POST['password'])
			#comm = Communities.objects.get(name='users')
			profile = UserProfile(user=user)
			profile.save()
			user = authenticate(username=request.POST['user_reg'], password=request.POST['password'])
			auth.login(request, user)
			return render(request,'user/profile.html')
	else:
		return render(request,"user/login.html")

def login(request):
	if request.method == 'POST':
		#if User.objects.filter(username=request.POST['username']).exists():
		user = authenticate(username=request.POST['username'], password=request.POST['password'])
		if user is not None:
			auth.login(request, user)
			return redirect('/')
		else:
			return render(request, 'user/login.html',{'pwd_x':" <h4 class='text-danger'>Username and password don't match.</h4>"})
	elif request.user.is_authenticated():
		return redirect('/')	
	else:
		return render(request,"user/login.html")

@login_required
def logout(request):
	if request.user.is_authenticated() == False:
		return render(request, 'user/login.html',{'msg':" <h4 class='text-danger'>Do login to continue.</h4>"})
	auth.logout(request)
	return render(request, 'user/login.html',{'msg':" <h4 class='text-success'>You have been successfully logged out.</h4>"})
	
@login_required
def profile(request):
	user = User.objects.get(username=request.user)
	#If signed up via OAuth
	if UserProfile.objects.filter(user__username=request.user).exists() == False:
		profile = UserProfile(user=request.user)
		profile.save()
	profile = UserProfile.objects.get(user=request.user)
	existing_files = profile.files_set.all().order_by('file_name').values()
	friend_requests = profile.requests.all().values('req_from__user__username')
	#comm = profile.comm
	return render(request, 'user/profile.html',{'user':user,'profile':profile,
		'file':existing_files,'friend_requests':friend_requests})
@require_POST
@login_required
def profile_update(request):
	user = User.objects.get(username=request.user)
	profile = UserProfile.objects.get(user__username=user)
	user.email = request.POST['email']
	user.first_name = request.POST['first_name']
	user.last_name = request.POST['last_name']
	user.save()
	profile.phone_no = request.POST['phone_no']
	profile.room_no = request.POST['room_no']
	profile.save()
	return HttpResponse('Success')

@login_required
def user(request, username):
	profile = get_object_or_404(UserProfile,user__username=username)
	existing_files = profile.files_set.all().order_by('file_name').values()
	user = UserProfile.objects.get(user=request.user)
	if profile in user.friends.all():
		friends = True
	else:
		friends = False
	#Has a friend request been sent to the request user from the current user?
	if user.req_from.filter(req_from=user,req_to=profile).exists():
		request_sent = True
	else:
		request_sent = False

	return render(request, 'user/user.html',{'user':profile,'profile':profile,
		'file':existing_files,'friends':friends,'request_sent':request_sent})

@login_required
def comm(request, name):
	comm = get_object_or_404(Communities, name=name)
	return render(request, 'user/comm.html',{'comm':comm})

@require_POST
@login_required
def comm_add(request):
	if Communities.objects.filter(name=request.POST['comm_name']).exists() == False:
		a = Communities(name=request.POST['comm_name'],description=request.POST['comm_desc'],private=request.POST['comm_private'],
			url=request.POST['comm_url'], code=request.POST['comm_code'])
		if a.private == False:
			a.code=''
		a.save()
		return redirect('/profile')
	else:
		return redirect('/profile')

@require_POST
@login_required
def comm_join(request):
	if Communities.objects.filter(name=request.POST['comm_name']).exists() == False:
		a = Communities(name=request.POST['comm_name'],description=request.POST['comm_desc'],private=request.POST['comm_private'],
			url=request.POST['comm_url'], code=request.POST['comm_code'])
		if a.private == False:
			a.code=''
		a.save()
		return redirect('/profile')
	else:
		return redirect('/profile')
@require_POST
@login_required
def friend_add(request):
	a= UserProfile.objects.get(user__username=request.user)
	b = UserProfile.objects.get(user__username=request.POST['req_to'])
	c = FriendRequests(req_from=a,req_to=b)
	c.save()
	return HttpResponse('sexy')

@require_POST
@login_required
def friend_accept(request):
	a = UserProfile.objects.get(user__username=request.user)
	b = UserProfile.objects.get(user__username=request.POST['req_from'])
	a.friends.add(b)
	a.save()
	c = FriendRequests.objects.get(req_from=b,req_to=a)
	c.delete()
	return HttpResponse('sexy')