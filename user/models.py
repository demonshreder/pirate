from django.db import models
from django.contrib.auth.models import User
# Create your models here.
class UserProfile(models.Model):
	user = models.OneToOneField(User,related_name='profile',on_delete=models.CASCADE)
	room_no = models.CharField(max_length=200)
	phone_no = models.CharField(max_length=200)
	comm = models.ManyToManyField('Communities')
	friends = models.ManyToManyField("self")

	def __str__(self):
		return self.user.username

class Communities(models.Model):
	name = models.CharField(max_length=200)
	description = models.TextField(null=True,default='')
	private = models.BooleanField(default=True)
	code = models.CharField(max_length=200,null=True)
	url = models.CharField(max_length=200,null=True)
	users = models.ManyToManyField('UserProfile')

	def __str__(self):
		return self.name

class FriendRequests(models.Model):
	req_from = models.ForeignKey(UserProfile, related_name='req_from',null=True)
	req_to = models.ForeignKey(UserProfile, related_name='requests',null=True)

	def __str__(self):
		return "From: " + self.req_from.user.username + " To: " + self.req_to.user.username