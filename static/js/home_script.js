var file_list = []

function escapeRegExp(str) {
  str = str.replace(
    /[\-\[\]\/\{\}\(\)\*\+\?\\\^\$\=\&\:\~\`\!\@\#\%\^\;\'\"\|]/g, '')
  // str = str.slice(0, -4)
  str = str.replace(/[\.]/g, ' ')
  str = str.replace(/[\_]/g, ' ')
  return str
}
// attribution: http://stackoverflow.com/questions/15900485/correct-way-to-convert-size-in-bytes-to-kb-mb-gb-in-javascript

function formatBytes(bytes, decimals) {
  if (bytes === 0) return '0 Byte'
  var k = 1000
  var dm = decimals + 1 || 3
  var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB']
  var i = Math.floor(Math.log(bytes) / Math.log(k))
  return (bytes / Math.pow(k, i)).toPrecision(dm) + ' ' + sizes[i]
}

function getCookie(name) {
  var cookieValue = null
  if (document.cookie && document.cookie !== '') {
    var cookies = document.cookie.split(';')
    for (var i = 0; i < cookies.length; i++) {
      var cookie = jQuery.trim(cookies[i])
      // Does this cookie string begin with the name we want?
      if (cookie.substring(0, name.length + 1) == (name + '=')) {
        cookieValue = decodeURIComponent(cookie.substring(name.length + 1))
        break
      }
    }
  }
  return cookieValue
}
var csrftoken = getCookie('csrftoken')
$(document).ready(function () {
  // code for search autocomplete
}); // end doc.ready
// code for displaying selected files
document.getElementById('dir_input').addEventListener('change', function () {
  $('#table_head').empty()
  $('#table_body').empty()
  // table_header = "<tr><th>Name</th><th>Size</th><th>Type</th><th>Don't want?</th></tr>"
  var table_header = '<tr><th>Name</th><th>Size</th><th>Type</th></tr>'
  $('#table_head').append(table_header)
  $('#caution').text('Unsupported formats will be ignored automatically.')
  $('#files_table').css('display', 'block')
  $('#step2').css('margin-top', '2em')
  for (var i = 0; i < this.files.length; i++) {

    var file = this.files[i]
    var file_type = file.name.slice(-3)
    var file_name = escapeRegExp(file.name)
    var total_types = ['dmg', 'iso', 'mkv', 'avi', 'mp3', 'mp4', 'cbr', 'pdf', '3gp', 'ogg', 'm4a', 'm4v', 'mov', 'flv',
      'mpg', 'vob', 'aac', 'dvx', ' 7z', 'rar', 'zip', 'pub', 'obi', 'peg', 'lac', ' gz', 'tar', 'deb']
    for (let value of total_types) {
      if (file_type == value) {
        window.file_list.push({ 'file_name': file_name, 'size': String(formatBytes(file.size)), 'type':file_type })
        var table_body = '<tr><td>' + file_name +
          "</td><td sorttable_customkey='" + file.size + "'>" + formatBytes(
            file.size) + '</td><td>' + file_type + '</td></tr>'
        $('#table_body').append(table_body)
      }
    }
  }
}, false)
    //   }
    //   var newTableObject = document.getElementById('list_files')
    //   sorttable.makeSortable(newTableObject)
    // // $(".toBeHidden").css("display":"block")
    // }, false)
    // Code for uploading
    document.getElementById('upload').addEventListener('click', function () {
      function csrfSafeMethod(method) {
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method))
      }
      $.ajaxSetup({
        beforeSend: function (xhr, settings) {
          if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader('X-CSRFToken', csrftoken)
          }
        }
      })
      $('#message').html(
        "<img src='/static/img/ring.gif' style='height:4em;width:4em'><br><br><h4 class='text-warning'>Listing in progress. Standby.....</h4>"
      )
      $.ajax({
        type: 'POST',
        url: 'upload',
        data: JSON.stringify(file_list),
        // dataType: "json",
        success: function success() {
          $('#message').html(
            "<h4 class='text-success'>Yay! All files have been listed. Thanks for pitching in.  &nbsp; &nbsp; &nbsp;  :)</h4>"
          )
          $('.toBeHidden').empty()
          $('#comments').css('display', 'block')
        }
      }) // Ajax end  
      // If end                                                           
    }, false)
