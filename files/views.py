#URL & shortcuts
from django.shortcuts import render, redirect
from django.http import HttpResponse
#DB Imports
from django.db import models
from files.models import files, stats, feedback, searches
from django.contrib.auth.models import User
from user.models import UserProfile, Communities
#Authentication
from django.contrib.auth import authenticate
from django.contrib.auth.decorators import login_required
#Querying
from ast import literal_eval as make_tuple
from django.utils import timezone
from datetime import date
import json
from django.db.models import Q
import random
#from django.core.urlresolvers import reverse
from django.contrib.staticfiles.templatetags.staticfiles import static

def test(request):
    return render(request,'files/home_logged.html')

#Homepage for the site
def index(request):
    if request.user.is_authenticated == False:
        return redirect('/login')
    if request.user.is_superuser == False or request.user.is_staff == False:
        try:
            views = stats.objects.get(date=date.today())
            views.hits = views.hits + 1
            views.save()
        except stats.DoesNotExist:
            views = stats(hits=1)
            views.save()
    return render(request,'files/home.html')

@login_required
def upload(request):
    kek = []
    for k in request.POST:
        kek = k
    kek.replace(kek[0],"")
    kek.replace(kek[-1],"")
    kek =  json.loads(kek)
    total_types = ['dmg', 'iso', 'mkv', 'avi', 'mp3', 'mp4', 'cbr', 'pdf', '3gp', 'ogg',  'm4a', 'm4v', 'mov', 'flv',
      'mpg', 'vob', 'aac', 'dvx', ' 7z', 'rar', 'zip','pub','obi','peg','flac', ' gz', 'tar', 'deb']
    tags = {'dmg':'Software', 'iso':'Software', ' 7z':'Software', 'rar':'Software', 'zip':'Software',' gz':'Software', 'tar':'Software', 'deb':'Software','mkv':'Videos', 'avi':'Videos',  'mp4':'Videos', '3gp':'Videos', 'm4v':'Videos', 'mov':'Videos', 'flv':'Videos','mpg':'Videos', 'vob':'Videos','aac':'Audio', 'dvx':'Videos','peg':'Videos','cbr':'Ebooks', 'pdf':'Ebooks', 'pub':'Ebooks','obi':'Ebooks','lac':'Audio','ogg':'Audio','m4a':'Audio','mp3':'Audio'}
    user = UserProfile.objects.get(user__username__exact=request.user)
    existing_files = user.files_set.all()
    #Test cases for quality control
    for i in kek:
        file_type = ''
        #File type check via extension
        if i['file_name'][-3:] in total_types:
            file_type = i['file_name'][-3:]
            i['file_name'] = i['file_name'][:-4]
        elif i['file_name'][-4:] in total_types:
            file_type = i['file_name'][-4:]
            i['file_name'] = i['file_name'][:-5]
        #This has become ineffective with pg_trgm search
        else:
            continue
        if existing_files.filter(file_name__exact=i['file_name']).exists():
            continue
        #Eliminating small files
        if str(i['size'])[-2:] not in ['MB','GB','TB']:
            continue
        #Eliminating keywords
        if 'sample' in i['file_name']:
            continue
        b = files(file_name=str(i['file_name']),size=str(i['size']),user=user,file_type=str(file_type))
        b.save()
    return render (request, 'files/search.html', {'post':"<h3 class='success-text'>Yay! All files have been listed. Thanks for pitching in. :)</h3>"})

def search(request):
    #init
    query=request.POST['search'].lower().strip()
    existing_files = files.objects.all()
    result = []
    peeps = []
    comms = []
    tag = request.POST['tag']
    #Not random
    if query != "":
        if tag == 'all':
            result = files.objects.filter(file_name__icontains=query)[:100].values('file_name','file_type','size','user__user__first_name','user__user__last_name','user__user__email','user__user__username','user__phone_no','user__room_no')
        #Sorting the results based on queries
        else:
            #Equating the number of queries based on tags given
            equate = 100//len(tag)
            for i in tag:
                result += files.objects.filter(file_name__icontains=query,file_type__icontains=tag)[:equate].values('file_name','file_type','size','user__user__first_name','user__user__last_name','user__user__email','user__user__username','user__phone_no','user__room_no')
    #Random cuz the 'query' in POST data is empty
    else:
        id_list = existing_files.values('id')
        count = files.objects.count()
        count = count
        for i in range(100):
            id = random.randint(0,count)
            try:
                a = existing_files.filter(id=id_list[id]['id']).values('file_name','file_type','size','user__user__first_name','user__user__last_name','user__user__email','user__user__username','user__phone_no','user__room_no')
                for b in a:
                    result.append(b)
            except:
                continue
    #No files found
    if not result:
        if request.user.is_superuser == False or request.user.is_staff == False:
            search_hit = searches(date=timezone.now(),query=query,listed=False)
            search_hit.save()
        #Doge pic cuz search failed
        return render(request,'files/search.html',{'doge':static('img/doge'+ str(random.randint(1,17)) +'.jpg'),'file':result,'peeps':peeps,'comms':comms})
    #Yay, files are found
    else:
        if request.user.is_superuser == False or request.user.is_staff == False:
            search_hit = searches(date=timezone.now(),query=query,listed=True)
            search_hit.save()
        return render(request,'files/search.html',{'file':result,'peeps':peeps,'comms':comms})

#Captain's page
def captain(request):
    if request.user.is_superuser:
        search_dict = searches.objects.all()
        users = User.objects.all()
        file_count = files.objects.count()
        hits = stats.objects.all().order_by('-date')[:100].values()
        kids = users.count()
        total_view =  stats.objects.aggregate(models.Sum('hits'))
        total_search =  search_dict.count()
        comm =  Communities.objects.count()
        listed =  search_dict.filter(listed='True').count()
        not_listed =  search_dict.filter(listed='False').count()
        msgs = feedback.objects.all().order_by('-date').values('date','msg','name','user__user__first_name','user__user__last_name')[:10]
        queries = search_dict.order_by('-date').values('query','date','listed')[:100]
        users = users.order_by('-date_joined').values('username','first_name','last_name','email','date_joined')[:10]
        return render(request, 'files/admin.html',{'hits':hits,'files':file_count,'kids':kids,'total_view':total_view['hits__sum'],
            'queries':queries,'total_search':total_search,'listed':listed,'not_listed':not_listed,'feedback':msgs,'comm':comm,'users':users})
    else:
       return redirect("/")


#Method to store feedback taken after listing and searches
def review(request):
    profile = UserProfile.objects.get(user=request.user)
    a = request.POST
    msg = feedback(name=a['name'],msg=a['msg'],date=timezone.now(),user=profile)
    msg.save()
    return render(request,"files/home   .html",{'msg':'<h4 class="text-success">Your feedback has been successfully saved. Thank you.</h4>'})

#Method to delete an individual file from the profile page
@login_required
def file_delete(request):
    file_deleted = files.objects.all().get(id=request.POST['file_id'])
    file_deleted.delete()
    profile = UserProfile.objects.get(user=request.user)
    return HttpResponse(profile.files_set.all().count())

#Method to delete all files from the profile page
@login_required
def file_delete_all(request):
    profile = UserProfile.objects.get(user=request.user)
    profile.files_set.all().delete()
    profile.save()
    return redirect('/profile')

@login_required
def file_update(request):
    file_updated = files.objects.all().get(id=request.POST['file_id'])
    file_updated.file_name = request.POST['file_name']
    file_updated.save()
    return HttpResponse('sexy')
    
def auto_search(request,query):
    query = list(files.objects.filter(file_name__icontains=query).order_by('file_name').values_list('file_name')[:10])
   # query = files.objects.filter(file_name__icontains=query).order_by('file_name')[:10]
    #query = str(query)
    #query = query.join(',')
    #query.replace(query[0],"")
    #query.replace(query[-1],"")
    #query = query.split(')(')
   # query = json.dumps(query)
   # query = serializers.serialize('json',query)
    return HttpResponse(query, content_type='application/json')