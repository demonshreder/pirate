# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('files', '0004_auto_20160112_2259'),
    ]

    operations = [
        migrations.AddField(
            model_name='files',
            name='date',
            field=models.DateTimeField(auto_now_add=True, default=datetime.datetime(2016, 1, 13, 17, 9, 58, 774348, tzinfo=utc)),
            preserve_default=False,
        ),
    ]
