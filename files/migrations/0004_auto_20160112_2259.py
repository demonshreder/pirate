# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('user', '0002_auto_20160112_2259'),
        ('files', '0003_remove_searches_logged_in'),
    ]

    operations = [
        migrations.AddField(
            model_name='feedback',
            name='user',
            field=models.ForeignKey(blank=True, null=True, to='user.UserProfile'),
        ),
        migrations.AddField(
            model_name='searches',
            name='user',
            field=models.ForeignKey(blank=True, null=True, to='user.UserProfile'),
        ),
    ]
