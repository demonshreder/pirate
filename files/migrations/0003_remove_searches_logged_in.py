# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('files', '0002_searches_logged_in'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='searches',
            name='logged_in',
        ),
    ]
