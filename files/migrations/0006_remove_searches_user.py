# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('files', '0005_files_date'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='searches',
            name='user',
        ),
    ]
