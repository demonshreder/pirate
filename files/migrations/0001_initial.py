# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('user', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='feedback',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('date', models.DateTimeField(auto_now_add=True)),
                ('name', models.CharField(max_length=200)),
                ('msg', models.TextField()),
            ],
        ),
        migrations.CreateModel(
            name='files',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('file_name', models.TextField()),
                ('size', models.CharField(max_length=20)),
                ('file_type', models.CharField(max_length=20)),
                ('user', models.ForeignKey(to='user.UserProfile')),
            ],
        ),
        migrations.CreateModel(
            name='searches',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('date', models.DateTimeField(auto_now_add=True)),
                ('query', models.CharField(max_length=200)),
                ('listed', models.BooleanField(default=False)),
            ],
        ),
        migrations.CreateModel(
            name='stats',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('hits', models.IntegerField()),
                ('date', models.DateField(auto_now_add=True)),
            ],
        ),
    ]
