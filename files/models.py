from django.db import models
from django.contrib.auth.models import User
from datetime import date
from django.utils import timezone
#from user.models import UserProfile
# Create your models here.

class files(models.Model):
    file_name = models.TextField()
    size = models.CharField(max_length=20)
    user = models.ForeignKey('user.UserProfile',on_delete=models.CASCADE)
    file_type = models.CharField(max_length=20)
    date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.file_name

class stats(models.Model):
	hits = models.IntegerField()
	date = models.DateField(auto_now_add=True)

class feedback(models.Model):
    date = models.DateTimeField(auto_now_add=True)
    name = models.CharField(max_length=200)
    msg = models.TextField()
    user = models.ForeignKey('user.UserProfile',null=True, blank=True)

class searches(models.Model):
    date = models.DateTimeField(auto_now_add=True)
    query = models.CharField(max_length=200)
    listed = models.BooleanField(default=False)
