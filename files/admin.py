from django.contrib import admin
from files.models import files,stats,feedback,searches
# Register your models here.
admin.site.register(files)
admin.site.register(stats)
admin.site.register(feedback)
admin.site.register(searches)
