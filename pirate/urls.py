"""pirate URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib import admin
import files.views
import user.views

urlpatterns = [
    #Admin Site
    #url(r'^grappelli/', include('grappelli.urls')), # grappelli URLS
    url(r'^boss/', include(admin.site.urls),name='boss'),
    #Home & Captain
    url(r'^$', files.views.index, name='home'),
    url(r'^review$', files.views.review, name ='review'),
    url(r'^captain$', files.views.captain, name='captain'),
    #Text pages
    url(r'^policy$', user.views.policy, name='policy'),
    url(r'^about$', user.views.about, name='about'),
    #Files
    url(r'^upload$', files.views.upload, name='upload'),
    url(r'^search$', files.views.search, name='search'),
    url(r'^file_delete$', files.views.file_delete, name='file_delete'),
    url(r'^file_delete_all$', files.views.file_delete_all, name='file_delete_all'),
    url(r'^file_update$', files.views.file_update, name='file_update'),
    #Communities
    url(r'^comm/(?P<name>[a-z0-9A-Z \.\,\_\-\$\~\'\@\!\#\%\^\&\*]+)$', user.views.comm, name='comm'),
    url(r'^comm_add$', user.views.comm_add, name='comm_add'),
    url(r'^comm_join$', user.views.comm_join, name='comm_join'),
    #Friends
    url(r'^friend_add$', user.views.friend_add, name='friend_add'),
    url(r'^friend_accept$', user.views.friend_accept, name='friend_accept'),
    #User*(\([a-zA-Z]+\).+)
    url(r'^user/(?P<username>[a-z0-9A-Z \.\,\_\-\$\~\'\@\!\#\%\^\&\*]+)$', user.views.user, name='user'),
    url(r'^login$', user.views.login, name='login'),
    url(r'^register$', user.views.register, name='register'),
    url(r'^logout$', user.views.logout, name='logout'),
    url(r'^profile$', user.views.profile, name='profile'),
    url(r'^profile_update$', user.views.profile_update, name='profile_update'),
    url(r'^account_reset_passwowrd$', files.views.index, name='account_reset_password'),
    #Testing
    url(r'^test$', files.views.test, name ='test'),
    url(r'^test2$', user.views.test2, name ='test2'),
    #Autocomplete
    url(r'^autocomplete/search/(?P<query>[\\?\\=a-z0-9 ]+)$', files.views.auto_search, name='auto_search'),
]