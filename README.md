# Pirate

Pirate is an online media files listing directory.

  - Directory of nearby people & friends with files
  - 3 click share or Drag 'n' Drop
  - Advanced search suggests

Pirate is written in Django.

Check current patch.txt in every commit to know the details.

### Version
Beta 0.5

### Development

#### This repo

Want to contribute? Great! Send us a pull request. PM if you want to see upcoming features list.

### License

AGPLv3

This means everything you write in this repository should be released under the GPLv3 license

https://choosealicense.com/licenses/agpl-3.0/
https://www.gnu.org/licenses/gpl-faq.html
